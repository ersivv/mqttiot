Собирается с помощью [PlatformIO](https://platformio.org/) в [Visual Code](https://code.visualstudio.com/).

В скетче необходимо указать параметры **WiFi** и брокера **MQTT**.

Регистрируемся на [cloudmqtt](https://www.cloudmqtt.com/)

Создаем инстанс с бесплатным планом [Cute Cat](https://customer.cloudmqtt.com/instance/create?plan=cat)

Настройки подключения к брокеру

![Настройки подключения](doc/img/mqtt_connect.png)

События можно видеть на вкладке **Websocket UI**.

![События](doc/img/mqtt_console.png)

Устанавливаем [Mqtt Dash](https://play.google.com/store/apps/details?id=net.routix.mqttdash&hl=ru)

Настраиваем подключение к серверу

![Настройки сервера](doc/img/mqtt_dash_server_1.jpg)

Указываем учетные данные и выставляем крупный размер плиток и отображение в один столбец

![Настройки сервера](doc/img/mqtt_dash_server_2.jpg)

Добавляем виджет **Текст**

![Настройки виджета](doc/img/mqtt_dash_widget_0.jpg)

Указываем название, подписку на топик, крупный размер, постфикс

![Настройки виджета](doc/img/mqtt_dash_widget_1.jpg)

Для дополнительных датчиков меняем индекс в окончании топика

Получаем результат

![Результат](doc/img/mqtt_dash_result.jpg)


