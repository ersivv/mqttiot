#include <array>
#include <vector>
#include <map>
#include <functional>
#include <algorithm>

#include <Arduino.h>

#include <pgmspace.h>
#include <OneWire.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// #include "sntp.h"
// #include <time.h>      // time() ctime()
// #include <sys/time.h>  // struct timeval
// #include <coredecls.h> // settimeofday_cb()

#include <DallasTemperature.h>
#include <FS.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "AsyncJson.h"
#include "ArduinoJson.h"
#include <AsyncMqttClient.h>
#include <Ticker.h>


const char* ssid = "ssid";
const char* password = "password";

const char *mqtt_server = "mqtt_server"; // Имя сервера MQTT
const int mqtt_port = mqtt_port; // Порт для подключения к серверу MQTT
const char *mqtt_user = "mqtt_user"; // Логи от сервер
const char *mqtt_pass = "mqtt_pass"; // Пароль от сервера

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 0

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// arrays to hold device addresses
DeviceAddress insideThermometer;
uint8_t sensors_count = 0;
float temperature[10];
float temperature_old[10];

const char p_space PROGMEM = '\n';

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        if (deviceAddress[i] < 16)
            Serial.print("0");
        Serial.print(deviceAddress[i], HEX);
    }
}

void temperatureLoop() {

    sensors.requestTemperatures();
    for(uint8_t i = 0; i < sensors_count; i++) {
      temperature_old[i] = temperature[i];
      temperature[i] = sensors.getTempCByIndex(i);
      if(temperature[i] != temperature_old[i]) {
        char array[6];
        sprintf(array, "%.1f", temperature[i]);
        char temp[10];
        sprintf(temp, "/temp/%d", i);
        mqttClient.publish(temp, 0, true, array);
      }
    }
}

AsyncWebServer server(80);

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}




void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.printf("WiFi Failed!\n");
      return;
  }

  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Hostname: ");
  Serial.println(WiFi.hostname());
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi.");
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi.");
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}



void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  // Serial.print("Session present: ");
  // Serial.println(sessionPresent);
  // // uint16_t packetIdSub = mqttClient.subscribe("test/lol", 2);
  // // Serial.print("Subscribing at QoS 2, packetId: ");
  // // Serial.println(packetIdSub);
  // mqttClient.publish("test/lol", 0, true, "test 1");
  // Serial.println("Publishing at QoS 0");
  // uint16_t packetIdPub1 = mqttClient.publish("test/lol", 1, true, "test 2");
  // Serial.print("Publishing at QoS 1, packetId: ");
  // Serial.println(packetIdPub1);
  // uint16_t packetIdPub2 = mqttClient.publish("test/lol", 2, true, "test 3");
  // Serial.print("Publishing at QoS 2, packetId: ");
  // Serial.println(packetIdPub2);
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}


void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void setup()
{
    Serial.begin(115200);
    Serial.print(p_space);
    printf("%s %s GMT", __DATE__, __TIME__);
    Serial.print("starting \n");

    SPIFFS.begin();

    wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
    wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    // mqttClient.onSubscribe(onMqttSubscribe);
    // mqttClient.onUnsubscribe(onMqttUnsubscribe);
    // mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    mqttClient.setServer(mqtt_server, mqtt_port);
    mqttClient.setCredentials(mqtt_user,mqtt_pass);

    connectToWifi();



    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Hello, world");
    });

    server.on("/temperature", HTTP_ANY, [](AsyncWebServerRequest * request) {
        AsyncJsonResponse * response = new AsyncJsonResponse();
        JsonObject& root = response->getRoot();
        root[F("heap")] = ESP.getFreeHeap();
        // root[F("temp")] = temperature;
        response->setLength();
        request->send(response);
    });

    server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");
    server.onNotFound(notFound);

    server.begin();

    // Start up the library
    sensors.begin();
    sensors.setWaitForConversion(false);

    // locate devices on the bus
    sensors_count = sensors.getDeviceCount();
    Serial.print("Found ");
    Serial.print(sensors_count, DEC);
    Serial.println(" devices.");
    sensors.requestTemperatures();

    ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();

}

void loop()
{
    ArduinoOTA.handle();
    temperatureLoop();
}